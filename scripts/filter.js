const data = require("./data.js");

// const dataFilter = data.filter( search => search.favoriteFruit === "banana" );

// console.log(dataFilter)

// function data 1
const favBanana = () => {
  // Tempat penampungan hasil
  const result = [];

  for (let i = 0; i < data.length; i++) {
    // age nya dibawah 30 tahun dan favorit buah nya pisang
    if (data[i].age < 30 && data[i].favoriteFruit === "banana") {
      result.push(data[i]);
    }
  }
  return result.length < 1 ? [{message : "data kosong"}] : result
};

// // function data 2
const femaleOrFsw = () => {
  // Tempat penampungan hasil
  const result = [];

  for (let i = 0; i < data.length; i++) {
    // gender nya female atau company nya FSW4 dan age nya diatas 30 tahun
    if ((data[i].gender === "female" || data[i].company === "FSW4") && data[i].age > 30) {
      result.push(data[i]);
    }
  }
  return result.length < 1 ? [{message : "data kosong"}] : result
};

// function data 3
const colorBlueApple = () => {
  // Tempat penampungan hasil
  const result = [];

  for (let i = 0; i < data.length; i++) {
    // warna mata biru dan age nya diantara 35 sampai dengan 40, dan favorit buah nya apel
    if (data[i].eyeColor === "blue" && data[i].age >= 35 && data[i].age <= 40 && data[i].favoriteFruit === "apple") {
      result.push(data[i]);
    }
  }

  return result.length < 1 ? [{message : "data kosong"}] : result
};

// function data 4
const companyPelangiOrIntel = () => {
  // Tempat penampungan hasil
  const result = [];

  for (let i = 0; i < data.length; i++) {
    // company nya Pelangi atau Intel, dan warna mata nya hijau
    if ((data[i].company === "Pelangi" || "Intel") && data[i].eyeColor === "green") {
      result.push(data[i]);
    }
  }

  return result.length < 1 ? [{message : "data kosong"}] : result
};

// function data 5
const registeredActive = () => {
  // Tempat penampungan hasil
  const result = [];

  for (let i = 0; i < data.length; i++) {
    // registered di bawah tahun 2016 dan masih active(true)
    if (data[i].registered < "2016-10-11T12:25:56 -07:00" && data[i].isActive) {
      result.push(data[i]);
    }
  }

  return result.length < 1 ? [{message : "data kosong"}] : result
};


module.exports = { favBanana, femaleOrFsw, colorBlueApple, companyPelangiOrIntel, registeredActive };
