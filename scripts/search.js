const search = (data, search, key) => {
  if (search == undefined || search == "" || key == "") {
    return data;
  }

  const query = search == "" ? search : search.toLowerCase();

  const set = data.filter((el) => {
    if (key == "favoriteFruit") {
      return el.favoriteFruit == query;
    } else if (key == "eyeColor") {
      return el.eyeColor == query;
    } else if (key == "age") {
      return el.age == query;
    } else if (key == "gender") {
      return el.gender == query;
    } else if (key == "company") {
      return el.company.toLowerCase() == query;
    }
  });

  // console.log(set)
  return set;
};

module.exports = search;
