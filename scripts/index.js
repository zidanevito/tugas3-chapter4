// import expressjs
const express = require("express");
const app = express();
const port = 3000;
const data = require("./data");
const path = require('path');
const search = require('./search.js')

// import fungsi filter
const { favBanana, femaleOrFsw, colorBlueApple, companyPelangiOrIntel, registeredActive } = require("./filter");

// pake ejs
app.set("view engine", "ejs");
app.set('views', path.join(__dirname, '../public'));

app.get("/", (req, res) => {
  res.render("index");
});

app.get("/about", (req, res) => {
  res.render("about");
});

app.get("/data", (req, res) => {
  let filter = search(data, req.query.search, req.query.key)
  res.render("data", { filter });
});

// app.get("/data/:search", (req, res) => {
//   console.log(req.params.search)
//   // masih error
//   if (req.params.search === "data") {
//     dataFilter =  data;
//   }
//   else if (req.params.search === "favbanana") {
//     dataFilter =  favBanana();
//   }
//   else if (req.params.search === "femaleorfsw") {
//     dataFilter =  femaleOrFsw();
//   }
//   else if (req.params.search === "colorblueapple") {
//     dataFilter = colorBlueApple();
//   }
//   else if (req.params.search === "companypelangiorintel") {
//     dataFilter =  companyPelangiOrIntel();
//   }
//   else if (req.params.search === "registeredactive") {
//     dataFilter =  registeredActive();
//   }
//   else {
//     dataFilter = []
//   }
  
  // res.render("data", {
  //   data : dataFilter,
  // }); 

  // let search = (req.query)
  // let filter = dataFilter(search);
  // console.log(filter)
  // res.send({filter,});

  // let filter = dataFilter(data, req.params.search);
  // res.send({filter,});

  
// });

app.get("/data1", (req, res) => {
  let total = favBanana().length,
    data = favBanana();
  res.json({
    total,
    data,
  });
});

app.get("/data2", (req, res) => {
  let total = femaleOrFsw().length,
    data = femaleOrFsw();
  res.json({
    total,
    data,
  });
});

app.get("/data3", (req, res) => {
  let total = colorBlueApple().length,
    data = colorBlueApple();
  res.json({
    total,
    data,
  });
});

app.get("/data4", (req, res) => {
  let total = companyPelangiOrIntel().length,
    data = companyPelangiOrIntel();
  res.json({
    total,
    data,
  });
});

app.get("/data5", (req, res) => {
  let total = registeredActive().length,
    data = registeredActive();
  res.json({
    total,
    data,
  });
});

app.use("/", (req, res) => {
  res.status(404);
  res.render("404");
});

app.listen(port, () => {
  console.log(`Server sudah berjalan, silahkan buka http://localhost:${port}`);
});
